FROM node:16-alpine

WORKDIR /app

COPY . /app/

RUN apk add --no-cache python3 make g++
RUN yarn

EXPOSE 3000

CMD ["yarn", "dev"]
